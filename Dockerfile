FROM python:3

RUN pip install --no-cache-dir --upgrade pip && \
    pip --default-timeout=1000 install --no-cache-dir --force-reinstall -v "pandas==1.2.5"

CMD ["sleep infinity"]