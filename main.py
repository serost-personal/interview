import os
import json
import boto3

def lambda_handler(event, context):
    s3 = boto3.client('s3') # will read from env 
    s3_resource = boto3.resource('s3')

    for object in s3_resource.Bucket('interview-bucket-thing').objects.all():
        print(object.key)
        
    return json.dumps([i.key for i in s3_resource.Bucket('interview-bucket-thing').objects.all()], default=str)